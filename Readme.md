﻿# CTF WRITEUPS with Files
|                |Category                   |Question                         |
|----|-------------------------------|------------------------------------------|
|/STM CTF'18|`Mobile`               |Gizli Notlar                  |
|/STM CTF'18|`Mobile`               |İKİ BİN KIRK SEKİZE KADAR                  |
|/STM CTF'18|`Mobile`               |KolaySoruYahu                  |
|/STM CTF'18|`Mobile`               |Signal                  |
|/STM CTF'18|`OSINT`               |Bu Sene Flag Oldu                 |
|/STM CTF'18|`OSINT`               |Catch me If You Can                  |
|/STM CTF'18|`OSINT`               |Konferanstan önce bizde                  |
|/STM CTF'18|`OSINT`               |The Rumble in the Jungle                  |
|/STM CTF'18|`PWN`               |PWN1                  |
|/STM CTF'18|`PWN`               |Papapawn                  |
|/STM CTF'18|`Reverse`               |JoS-Beni|
|/STM CTF'18|`Reverse`               |8bitKaplumbagaCNC|
|/STM CTF'18|`Reverse`               |Kaynak Kodu Benden|
|/STM CTF'18|`Reverse`               |Matruşka|
|/STM CTF'18|`Reverse`               |parolaLutfen|
|/STM CTF'18|`Reverse`               |Tersinego|
|/STM CTF'18|`Web`               |dropbox|
|/STM CTF'18|`Web`               |Gönder Gelsin|
|/STM CTF'18|`MISC`               |Freenode|
|/STM CTF'18|`MISC`               |Gitmek İstiyorum|
|/STM CTF'18|`MISC`               |H4ckl3nd1k|
|/STM CTF'18|`MISC`               |Lorem Ipsum|
|/STM CTF'18|`MISC`               |MANZARA|
|/STM CTF'18|`MISC`               |Stmlogo|
|/STM CTF'18|`Malware`               |DA2|
|/STM CTF'18|`Malware`               |DiEfAyAr|
|/STM CTF'18|`FORENSICS`               |Demir Adamın Günlükleri|
|/STM CTF'18|`FORENSICS`               |Gizli Saklı|
|/STM CTF'18|`FORENSICS`               |Kolay Olsun Dedik|
|/STM CTF'18|`FORENSICS`               |LOKI BITmiş|
|/STM CTF'18|`FORENSICS`               |Örümcek Analiz|
|/STM CTF'18|`FORENSICS`               |Parola olmuş Flag|
|/STM CTF'18|`CRYPTO`               |Badi|
|/STM CTF'18|`CRYPTO`               |On Parmak|
|/STM CTF'18|`CRYPTO`               |Rastgele|
|/STM CTF'18|`CRYPTO`               |RSA POOL|
|/STM CTF'18|`CRYPTO`               |STM128|
|/STM CTF'18|`Coding`               |BIPBIP|
|/STM CTF'18|`Coding`               |CIPHER101|
|/STM CTF'18|`Coding`               |Rengarenk String|